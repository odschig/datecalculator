# Date Calculator
It is built and tested with Java 1.8.0_181

## Decisions
* The application takes two arguments in "YYYY-MM-DD" format
* The order of the input dates should not change the result. The application will determine the beginning date and the ending date to calculate
* Regular expression is the simplest way to confirm the validity of date format
* Use JUnit for the unit testing. Most effective / proven way fo unit testing
* Use Java 8 Date API to confirm the full days calculation

## Calculation algorithm
* simple substraction of two days if two dates are in the same year and the same month
* if two dates are in the same yea but in the different monthr<br>
  (a) add entire days in each month between the beginning month and the ending month (excluding the ending month)<br>
  (b) add days in the ending month<br>
  (c) subsctract days in the beginning month
* if two dates are in different years<br>
  (a) add entire days from the begining month to the end of the beginning year<br>
  (b) add entire days to the ending month (excluding the ending month) of the ending year<br>
  (c) add entire days of the years in between (excluding both the beginning and the ending year)<br>
  (d) add days in the ending month<br>
  (e) subsctract days in the beginning month

## Project Structure
### Directories
* bin: compiled classes
* lib: libraries
* src: java code base

### Compile
``` bash
javac -d bin -cp lib/junit-4.10.jar:lib/hamcrest-core-1.3.jar src/io/raymond/*.java
```

### Test
``` bash
java -cp bin:lib/junit-4.10.jar:lib/hamcrest-core-1.3.jar org.junit.runner.JUnitCore io.raymond.DateCalculatorTest
java -cp bin:lib/junit-4.10.jar:lib/hamcrest-core-1.3.jar org.junit.runner.JUnitCore io.raymond.UtilsTest
```

### Run
* Usage: `io.raymond.DateCalculator YYYY-MM-DD YYYY-MM-DD`
* Example
``` bash
java -cp bin io.raymond.DateCalculator 1984-07-04 1984-12-25
```
