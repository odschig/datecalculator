package io.raymond;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;

public class DateCalculatorTest {
    public LocalDate fromDate, toDate;
    public DateTimeFormatter formatter;

    @Before
    public void init() {
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }

    @Test
    public void testcase01() {
        fromDate = LocalDate.parse("1983-06-02", formatter);
        toDate = LocalDate.parse("1983-06-22", formatter);
        DateCalculator dc = new DateCalculator(Utils.parseInputDate(fromDate.toString()),
                Utils.parseInputDate(toDate.toString()), false);
        assertThat(dc.calculateTotalDays(), equalTo(ChronoUnit.DAYS.between(fromDate, toDate) - 1));
    }

    @Test
    public void testcase01ReversedInput() {
        fromDate = LocalDate.parse("1983-06-02", formatter);
        toDate = LocalDate.parse("1983-06-22", formatter);
        DateCalculator dc = new DateCalculator(Utils.parseInputDate(toDate.toString()),
                Utils.parseInputDate(fromDate.toString()), true);
        assertThat(dc.calculateTotalDays(), equalTo(ChronoUnit.DAYS.between(fromDate, toDate) - 1));
    }

    @Test
    public void testcase02() {
        fromDate = LocalDate.parse("1984-07-04", formatter);
        toDate = LocalDate.parse("1984-12-25", formatter);
        DateCalculator dc = new DateCalculator(Utils.parseInputDate(fromDate.toString()),
                Utils.parseInputDate(toDate.toString()), false);
        assertThat(dc.calculateTotalDays(), equalTo(ChronoUnit.DAYS.between(fromDate, toDate) - 1));
    }

    @Test
    public void testcase02ReversedInput() {
        fromDate = LocalDate.parse("1984-07-04", formatter);
        toDate = LocalDate.parse("1984-12-25", formatter);
        DateCalculator dc = new DateCalculator(Utils.parseInputDate(toDate.toString()),
                Utils.parseInputDate(fromDate.toString()), true);
        assertThat(dc.calculateTotalDays(), equalTo(ChronoUnit.DAYS.between(fromDate, toDate) - 1));
    }

    @Test
    public void testcase03() {
        fromDate = LocalDate.parse("1979-01-03", formatter);
        toDate = LocalDate.parse("1983-08-03", formatter);
        DateCalculator dc = new DateCalculator(Utils.parseInputDate(fromDate.toString()),
                Utils.parseInputDate(toDate.toString()), false);
        assertThat(dc.calculateTotalDays(), equalTo(ChronoUnit.DAYS.between(fromDate, toDate) -1));
    }

    @Test
    public void testcase03ReversedInput() {
        fromDate = LocalDate.parse("1979-01-03", formatter);
        toDate = LocalDate.parse("1983-08-03", formatter);
        DateCalculator dc = new DateCalculator(Utils.parseInputDate(toDate.toString()),
                Utils.parseInputDate(fromDate.toString()), true);
        assertThat(dc.calculateTotalDays(), equalTo(ChronoUnit.DAYS.between(fromDate, toDate) - 1));
    }
    @Test
    public void testcase04() {
        fromDate = LocalDate.parse("1901-01-01", formatter);
        toDate = LocalDate.parse("2999-12-31", formatter);
        DateCalculator dc = new DateCalculator(Utils.parseInputDate(fromDate.toString()),
                Utils.parseInputDate(toDate.toString()), false);
        assertThat(dc.calculateTotalDays(), equalTo(ChronoUnit.DAYS.between(fromDate, toDate) -1));
    }
}
