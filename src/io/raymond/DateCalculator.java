package io.raymond;

import io.raymond.Utils;

public class DateCalculator {
    public static final String DATE_DELIM = "-";

    int yearFrom, monthFrom, dayFrom, yearTo, monthTo, dayTo;

    public DateCalculator(String[] firstArg, String[] secondArg, boolean isReversedOrder) {
        String[] dateFrom = (isReversedOrder) ? secondArg: firstArg;
        String[] dateTo = (isReversedOrder) ? firstArg: secondArg;

        yearFrom = Integer.parseInt(dateFrom[0]);
        monthFrom = Integer.parseInt(dateFrom[1]);
        dayFrom = Integer.parseInt(dateFrom[2]);
        yearTo = Integer.parseInt(dateTo[0]);
        monthTo = Integer.parseInt(dateTo[1]);
        dayTo = Integer.parseInt(dateTo[2]);
    }

    public String getDateFrom() {
        return String.format("%02d", yearFrom) + DATE_DELIM + String.format("%02d", monthFrom) + DATE_DELIM
                + String.format("%02d", dayFrom);
    }

    public String getDateTo() {
        return String.format("%02d", yearTo) + DATE_DELIM + String.format("%02d", monthTo) + DATE_DELIM
                + String.format("%02d", dayTo);
    }

    public long calculateTotalDays() {
        long totalDays = 0;
        if (yearFrom == yearTo) {
            if (monthFrom == monthTo) {
                totalDays = (dayTo - 1) - dayFrom;
            } else {
                for (int currMonth = monthFrom; currMonth < monthTo; currMonth++) {
                    totalDays += Utils.getLastDayInMonth(yearTo, currMonth);
                }
                totalDays += (dayTo - 1 - dayFrom);
            }
        } else {
            // calculate days of the first year
            for (int currMonth = monthFrom; currMonth < 13; currMonth++) {
                totalDays += Utils.getLastDayInMonth(yearFrom, currMonth);
            }
            // calculate days of the last year
            for (int currMonth = 1; currMonth < monthTo; currMonth++) {
                totalDays += Utils.getLastDayInMonth(yearTo, currMonth);
            }
            // calculate days of the years in between
            for (int currYear = yearFrom + 1; currYear < yearTo; currYear++) {
                totalDays += Utils.isLeapYear(currYear) ? 366 : 365;
            }
            totalDays += (dayTo - 1 - dayFrom);
        }

        return totalDays;
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: io.raymond.DateCalculator YYYY-MM-DD YYYY-MM-DD");
            System.exit(0);
        }

        if (!(Utils.isValidDateFormat(args[0]) && Utils.isValidDateFormat(args[1]))) {
            System.out.println("Invalid input format. Please enter the two dates in 'YYYY-MM-DD' format.");
            System.exit(0);
        }

        if (!(Utils.isValidDay(args[0]) && Utils.isValidDay(args[1]))) {
            System.out.println("Invalid days. Please enter valid days.");
            System.exit(0);
        }

        DateCalculator dc;
        String[] firstArg = Utils.parseInputDate(args[0]), secondArg = Utils.parseInputDate(args[1]);
        dc = new DateCalculator(firstArg, secondArg, Utils.isReversedOrder(firstArg, secondArg));
        
        System.out.printf("Total full days between %s and %s are %d\n", dc.getDateFrom(), dc.getDateTo(), dc.calculateTotalDays());
    }
}
