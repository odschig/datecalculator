package io.raymond;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UtilsTest {
    @Test
    public void testIsValidDateFormat01() {
        assertEquals(true, Utils.isValidDateFormat("1901-01-01"));
        assertEquals(true, Utils.isValidDateFormat("2999-12-31"));
    }

    @Test
    public void testIsValidDateFormat02() {
        assertEquals(false, Utils.isValidDateFormat("979-01-03"));
        assertEquals(false, Utils.isValidDateFormat("1899-12-31"));
        assertEquals(false, Utils.isValidDateFormat("3000-01-01"));
    }

    @Test
    public void testIsValidDateFormat03() {
        assertEquals(false, Utils.isValidDateFormat("2000-13-01"));
        assertEquals(false, Utils.isValidDateFormat("2000-08-41"));
        assertEquals(false, Utils.isValidDateFormat("2000-07-00"));
    }

    @Test
    public void testIsValidDay() {
        assertEquals(true, Utils.isValidDay("2000-08-31"));
        assertEquals(true, Utils.isValidDay("2000-02-29"));
        assertEquals(false, Utils.isValidDay("2000-09-31"));
        assertEquals(false, Utils.isValidDay("1900-02-29"));
    }

    @Test
    public void testIsReverseOrder01() {
        assertEquals(false,
                Utils.isReversedOrder(Utils.parseInputDate("1983-06-02"), Utils.parseInputDate("1983-06-22")));
        assertEquals(true,
                Utils.isReversedOrder(Utils.parseInputDate("1983-06-22"), Utils.parseInputDate("1983-06-02")));
    }

    @Test
    public void testIsReverseOrder02() {
        assertEquals(false,
                Utils.isReversedOrder(Utils.parseInputDate("1984-07-04"), Utils.parseInputDate("1984-12-25")));
        assertEquals(true,
                Utils.isReversedOrder(Utils.parseInputDate("1984-12-25"), Utils.parseInputDate("1984-07-04")));
    }

    @Test
    public void testIsReverseOrder03() {
        assertEquals(false,
                Utils.isReversedOrder(Utils.parseInputDate("1979-01-03"), Utils.parseInputDate("1983-08-03")));
        assertEquals(true,
                Utils.isReversedOrder(Utils.parseInputDate("1983-08-03"), Utils.parseInputDate("1979-01-03")));
    }
}
