package io.raymond;

import java.util.regex.Pattern;

public class Utils {
    public static final String DATE_PATTERN = "^(19|2\\d)\\d\\d-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";

    public static boolean isReversedOrder(String[] firstArg, String[] secondArg) {
        if (firstArg.length != 3 && secondArg.length != 3) {
            System.out.println("Warning: the date format is not valid.");
            return false;
        }
        if (Integer.parseInt(firstArg[0]) == Integer.parseInt(secondArg[0])) {
            if (Integer.parseInt(firstArg[1]) == Integer.parseInt(secondArg[1])) {
                return Integer.parseInt(firstArg[2]) > Integer.parseInt(secondArg[2]);
            }
            return Integer.parseInt(firstArg[1]) > Integer.parseInt(secondArg[1]);
        }
        return Integer.parseInt(firstArg[0]) > Integer.parseInt(secondArg[0]);
    }

    public static boolean isValidDateFormat(String dateString) {
        return Pattern.matches(DATE_PATTERN, dateString);
    }

    public static boolean isValidDay(String dateString) {
        String[] dateArray = dateString.split(DateCalculator.DATE_DELIM);
        return Integer.parseInt(dateArray[2]) <= Utils.getLastDayInMonth(Integer.parseInt(dateArray[0]), Integer.parseInt(dateArray[1]));
    }

    public static String[] parseInputDate(String dateString) {
        return dateString.split(DateCalculator.DATE_DELIM);
    }

    public static int getLastDayInMonth(int year, int month) {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return 31;
        } else if (month == 2) { // check leap year
            return Utils.isLeapYear(year) ? 29 : 28;
        } else {
            return 30;
        }    
    }    

    public static boolean isLeapYear(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }
}
